import { OAuth2Client, OAuth2Fetch } from '@badgateway/oauth2-client';

const client = new OAuth2Client({
  // The base URI of the OAuth2 server
  server: 'https://authentik.tooling.digilab.network/',

  // OAuth2 client id
  clientId: 'MXSoPMAzdICWsskd0ys523ieYBVHgwloH3f2D5xc', // TODO: in env/config

  // Token endpoint
  tokenEndpoint: '/application/o/token/',

  // Authorization endpoint
  authorizationEndpoint: '/application/o/authorize/',

  // OAuth2 Metadata discovery endpoint
  discoveryEndpoint: '/application/o/digilab-website/.well-known/openid-configuration',
});

const fetchWrapper = new OAuth2Fetch({
  client: client,

  getNewToken: async () => {
    return client.authorizationCode.getTokenFromCodeRedirect(
      document.location,
      {
        // The redirect URI below is not used for any redirects, but must be equal to the one passed to the authorizationCode flow
        redirectUri: `${location.origin}/callback`,

        // State, must be equal to the one sent to the authorizationCode flow
        state: localStorage.getItem('authState'),

        // codeVerifier for PKCE, must be equal to the one used in the authorizationCode flow
        codeVerifier: localStorage.getItem('codeVerifier'),
      }
    );
  },

  // This function is called whenever the active token changes. Useful for later usage
  storeToken: (token) => {
    localStorage.setItem('authToken', JSON.stringify(token));
  },

  // Function that attempts to use the stored token before doing a full re-authentication
  getStoredToken: () => {
    if (localStorage) {
      const token = localStorage.getItem('authToken');
      if (token) {
        return JSON.parse(token);
      }
    }
    return null;
  },


  // Error handling
  onError: (err) => {
    // err is of type Error
    console.log(err); // TODO: handle err
  },
});

async function run() {
  // Fetch the user info endpoint
  const response = await fetchWrapper.fetch('https://authentik.tooling.digilab.network/application/o/userinfo/', {
    method: 'GET',
  });

  const data = await response.json();

  const div = document.createElement('div');
  div.innerHTML = `<p>Ingelogd als:</p><h4>${data.name}</h4><p>${data.email}</p><img src="${data.picture}"><p class="mt-2"><a class="btn btn-primary" href="https://authentik.tooling.digilab.network/application/o/digilab-website/end-session/">Logout</a></p>`;
  document.getElementsByClassName('content')[0].appendChild(div);
}

run();
