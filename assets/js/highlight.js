import hljs from 'highlight.js/lib/core';

import javascript from 'highlight.js/lib/languages/javascript';
import json from 'highlight.js/lib/languages/json';
import bash from 'highlight.js/lib/languages/bash';
import xml from 'highlight.js/lib/languages/xml';
import ini from 'highlight.js/lib/languages/ini';
import yaml from 'highlight.js/lib/languages/yaml';
import markdown from 'highlight.js/lib/languages/markdown';
import python from 'highlight.js/lib/languages/python';

hljs.registerLanguage('javascript', javascript);
hljs.registerLanguage('json', json);
hljs.registerLanguage('bash', bash);
hljs.registerLanguage('html', xml);
hljs.registerLanguage('ini', ini);
hljs.registerLanguage('toml', ini);
hljs.registerLanguage('yaml', yaml);
hljs.registerLanguage('md', markdown);
hljs.registerLanguage('python', python);

document.addEventListener('DOMContentLoaded', () => {
  // Give all pre code blocks a tabindex="0" attribute, (or to the container <pre>s, which is somehow not done by default). See https://developer.mozilla.org/en-US/docs/Web/Accessibility/Understanding_WCAG/Keyboard and https://github.com/gohugoio/hugo/pull/8568
  document.querySelectorAll('pre code').forEach((block) => {
    block.setAttribute('tabindex', '0');
  });

  document.querySelectorAll('pre code:not(.language-mermaid)').forEach((block) => {
    hljs.highlightElement(block);
  });
});
