import { OAuth2Client, generateCodeVerifier } from '@badgateway/oauth2-client';

const client = new OAuth2Client({
    // The base URI of the OAuth2 server
    server: 'https://authentik.tooling.digilab.network/',

    // OAuth2 client id
    clientId: 'MXSoPMAzdICWsskd0ys523ieYBVHgwloH3f2D5xc', // TODO: in env/config

    // Token endpoint
    tokenEndpoint: '/application/o/token/',

    // Authorization endpoint
    authorizationEndpoint: '/application/o/authorize/',

    // OAuth2 Metadata discovery endpoint
    discoveryEndpoint: '/application/o/digilab-website/.well-known/openid-configuration',
});

async function run() {
    const state = await generateCodeVerifier();
    const codeVerifier = await generateCodeVerifier();

    // Store the state and codeVerifier in localStorage, to be checked after the redirect
    localStorage.setItem('authState', state);
    localStorage.setItem('codeVerifier', codeVerifier);

    // In a browser this might work as follows:
    document.location = await client.authorizationCode.getAuthorizeUri({
        // URL in the app that the user should get redirected to after authenticating
        redirectUri: `${location.origin}/callback`,

        // Optional string that can be sent along to the auth server. This value will be added to querystring of the redirect URL
        state: state,

        // Code verifier for PKCE
        codeVerifier,

        // Requested scopes
        scope: ['openid', 'profile', 'email'],
    });
}

run();
