module gitlab.com/digilab.overheid.nl/ecosystem/digilab-website

go 1.20

require (
	gitlab.com/digilab.overheid.nl/ecosystem/dcat-v2-validator/docs v0.0.0-20230814124007-497c22078b86 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/it-hygiene-validator v0.0.0-20230927121042-578eab99752a // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/terraform-modules/azure-generic-ipv6-proxy v0.0.0-20231025131558-7865e63bea15 // indirect
	gitlab.com/digilab.overheid.nl/research/event-sourcing/docs v0.0.0-20231011061359-167cd9ef6309 // indirect
)
