---
title: "Over"
description: ""
date: 2023-05-23T10:35:34+02:00
draft: false
images: []
---

De overheid heeft als doel om data breed en zo efficiënt mogelijk te delen in een Federatief Datastelsel. Met standaarden en afspraken over gegevensuitwisseling, waarbij data zoveel mogelijk bij de bron blijft. Digilab werkt samen met projecten die hieraan bijdragen, en biedt ze een stevige en vernieuwende ontwikkel- en testomgeving.

We zijn gestart met projecten en initiatieven die afkomstig zijn van de programma’s Federatief Datastelsel, Regie op Gegevens, Common Ground en Data bij de Bron. In principe is elk project dat bijdraagt aan één van de vraagstukken van deze programma’s een goede kandidaat om met ons mee te werken. De timing bepalen we samen.

## Meer informatie
[Interbestuurlijke Datastrategie en het Federatief Datastelsel](https://realisatieibds.pleio.nl/)

[Regie op Gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/)

[Common Ground](https://commonground.nl/)

[Data bij de Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/)
