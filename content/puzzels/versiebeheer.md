---
title: Versiebeheer
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T14:36:08.638Z
description: Het is van belang dat verschillende versies van de API-endpoints op
  een standaard manier worden beheerd.
tags:
  - Nieuws
---
Het is van belang dat verschillende versies van de API-endpoints op een standaard manier worden beheerd. Dit in verband met mogelijke breaking changes. Dit kan bijvoorbeeld door toepassen van semantic versioning en strikte regels rondom versionering, zoals het aantal versies dat tegelijkertijd onderhouden blijft en termijnen bij dat onderhoud.
