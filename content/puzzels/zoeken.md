---
title: Zoeken
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T14:23:36.459Z
description: Hier gaat het om de standaardisatie, want het kan al op talloze
  manieren; hoe doe je het, hoe zit het met parameters in URI's vs AVG?
tags:
  - Nieuws
---
Hier gaat het om de standaardisatie, want het kan al op talloze manieren; hoe doe je het, hoe zit het met parameters in URI's vs AVG? Er is een standaard te kiezen uit de diversiteit in de praktijk, een best practice waarmee in elke API op zelfde manier gezocht kan worden, etc.
