---
title: Autorisatie - allerlei patronen
draft: false
puzzel-categorie: Vertrouwen > Toegang
date: 2023-11-22T11:52:34.341Z
description: De uitdaging is om de autorisatie, per patroon te standaardiseren.
  Tussen overheid en bedrijfsleven, maar ook tussen overheid en burger.
tags: []
---
De uitdaging is om de autorisatie, per patroon te standaardiseren. Ten eerste gaat het om het interbestuurlijke patroon, tussen overheden. Daarnaast kan worden gedacht aan een patroon tussen overheden en bedrijfsleven, zoals ook in het vastgoeddomein tussen o.a. Kadaster, Belastingdienst enerzijds en notarissen, makelaars anderzijds. Of er kan sprake zijn van patroon tussen overheden en burgers , zoals bij overzicht van vorderingen aan de burger vanuit diverse overheidsinstanties.
