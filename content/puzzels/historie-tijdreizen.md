---
title: Historie/tijdreizen
draft: false
puzzel-categorie: Interoperabiliteit > Modellen
date: 2023-11-22T12:02:54.000Z
description: Er wordt gewerkt aan standaard-API's die deze functie gaan faciliteren.
tags: []
---
Het op kunnen halen van historische data, of de waarde van gegevens over een bepaalde periode noemen we ook wel 'tijdreizen'. Er wordt gewerkt aan standaard-API's die deze functie gaan faciliteren. Daarbij is de toepassing [eventsourcing](projecten/event-sourcing/) op bijvoorbeeld het beheer van basisregistraties ook een mogelijkheid om historie/tijdreizen binnen een federatief datastelsel mee te faciliteren.
