---
title: Technische interoperabiliteit
draft: false
puzzel-categorie: Vertrouwen > Veiligheid
date: 2023-11-22T15:03:20.295Z
description: Systemen zijn interoperabel als zij onderling kunnen communiceren
  en samenwerken, en als bij vervanging van één of meer van de  (deel)systemen
  geen verlies van functionaliteit of prestaties optreedt, óók niet als de
  vervangende (deel)systemen afkomstig zijn van andere leveranciers en/of
  ontworpen en gebouwd zijn op basis van andere principes.
tags: []
---
Applicaties dienen op alle aangesloten omgevingen eenvoudig in gebruik te kunnen worden genomen op een eenduidige manier. Systemen zijn interoperabel als zij onderling kunnen communiceren en samenwerken, en als bij vervanging van één of meer van de  (deel)systemen geen verlies van functionaliteit of prestaties optreedt, óók niet als de vervangende (deel)systemen afkomstig zijn van andere leveranciers en/of ontworpen en gebouwd zijn op basis van andere principes. Interoperabiliteit gaat verder dan uitwisselbaarheid, want van uitwisselbaarheid kan al gesproken worden als een (deel)systeem vervangen kan worden door eenzelfde entiteit. In praktische zin gaat het hier over o.a. Helm charts (package manager voor Kubernetes), Continuous Integration, deployments, standaardisaties (bijv. [Haven](projecten/haven/)) die leiden tot technische interoperabiliteit
