---
title: Authenticatie (organisaties, personen)
draft: false
puzzel-categorie: Vertrouwen > Identiteit
date: 2023-11-22T16:26:12.298Z
description: Standaardiseren van authenticatie, per patroon. Voor een patroon
  Business to Government (B2G) zijn andere standaarden nodig dan voor een
  patroon Government to Government (G2G)
tags: []
---
Standaardiseren van authenticatie, per patroon. Voor een patroon Business to Government (B2G) zijn andere standaarden nodig dan voor een patroon Government to Government (G2G)
