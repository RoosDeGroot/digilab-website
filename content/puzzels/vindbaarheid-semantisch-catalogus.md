---
title: Vindbaarheid (semantisch) / Catalogus
draft: false
puzzel-categorie: Datawaarde > Publicatie
date: 2023-11-22T13:14:40.641Z
description: Om de juiste data te vinden, moet deze herkenbaar zijn en doorzoekbaar.
tags: []
---
Om de juiste data te vinden, moet deze herkenbaar zijn en doorzoekbaar. Je moet kunnen zien waar over jouw zoekterm relevantie informatie is opgeslagen. Deze vindbaarheid heeft een technische en een inhoudelijke kant. Inhoudelijk wil je weten of de informatie inderdaad bij jouw vraag past. Daarvoor zijn semantische standaarden en metadatering nodig. Ook is er een catalogusfunctie nodig om deze betekenissen te doorzoeken.
