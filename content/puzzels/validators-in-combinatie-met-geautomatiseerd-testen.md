---
title: Validators (in combinatie met geautomatiseerd testen)
draft: false
puzzel-categorie: Vertrouwen > Toegang
date: 2023-11-22T13:55:34.355Z
description: Om compliancy te vergroten en vergemakkelijken kunnen validators en
  automatische testen helpen.
tags: []
---
Om compliancy te vergroten en vergemakkelijken kunnen validators en automatische testen helpen. Zo kan worden getoetst of een website aan de standaarden voldoet die worden gehanteerd. Voor de binnen Digilab te hanteren standaarden door projecten, als opmaat van standaarden voor deelname aan het Federatief Datastelsel, zijn deze validators een goede hulp om het ontwikkelen aan deze standaarden gecommiteerd te houden. Dit kan dan worden geoptimaliseerd door deze validators ook in het geautomatiseerde testen op te nemen.
