---
title: ACID/BASE afwegingen per patroon
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T14:51:44.627Z
description: Wanneer zet je voor databases ACID in (focus op consistentie)
  versus BASE (focus op high availability)?
tags: []
---
Een andere context kan vragen om andere afweging, soms is maximale consistentie belangrijker, soms availability (performance). Het is van belang de afwegingen hiertoe in kaart te brengen, zodat dit als afspraken kunnen gelden hoe organisaties hier (meer) uniform mee om kunnen gaan; wanneer zet je voor databases ACID in (focus op consistentie) versus BASE (focus op high availability).
