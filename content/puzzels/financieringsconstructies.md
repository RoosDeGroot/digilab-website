---
title: Financieringsconstructies
draft: false
puzzel-categorie: Governance > Bestuurlijk
date: 2023-11-22T10:29:46.919Z
description: Het zou helpen als er voorbeeld- en
  standaardfinancieringsconstructies zijn waar overheidsorganisaties gebruik van
  kunnen maken.
tags: []
---
Als er data wordt uitgewisseld, zijn daar kosten aan verbonden. Op dit moment zoekt elk project opnieuw uit hoe de kosten worden verdeeld en verrekend. Dit op zichzelf kost veel tijd en leidt tot uiteenlopende oplossingen. Om het principe Data bij de Bron te realiseren, is een andere wijze van financieren nodig dan wanneer data niet bij de bron hoeft te blijven. Het zou helpen als er voorbeeld- en standaardfinancieringsconstructies zijn waar overheidsorganisaties gebruik van kunnen maken.
