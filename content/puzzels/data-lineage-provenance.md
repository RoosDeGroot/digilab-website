---
title: Data lineage, provenance
draft: false
puzzel-categorie: Interoperabiliteit > Traceerbaarheid
date: 2023-11-22T13:21:25.465Z
description: Bij het ophalen van gegevens kan het relevant zijn om te weten uit
  welke bron(nen) deze afkomstig zijn. Bij het bepalen van de manier waarop deze
  medata wordt vastgelegd is een standaard noodzakelijk.
tags: []
---
Bij het ophalen van gegevens kan het relevant zijn om te weten uit welke bron(nen) deze afkomstig zijn. Bij het bepalen van de manier waarop deze medata wordt vastgelegd is een standaard noodzakelijk.
