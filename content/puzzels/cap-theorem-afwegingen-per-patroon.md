---
title: CAP theorem afwegingen per patroon
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T15:00:08.668Z
description: Door afwegingen in kaart te brengen kunnen die als afspraken gaan
  gelden in het Federatief Datastelsel hoe hiermee om te gaan.
tags: []
---
Volgens CAP theorem kan je 2 van de 3 wenselijke aspecten bereiken (consistentie, beschikbaarheid, tolerantie). Door afwegingen in kaart te brengen kunnen die als afspraken gaan gelden in het Federatief Datastelsel hoe hiermee om te gaan. Consistency gaat erover dat overal in het systeem dezelfde state aanwezig is.Availability gaat over het kunnen uitvoeren van updates. Partition tolerance gaat tenslotte over de vraag of een gedistribueerd systeem, als het door wegvallende communicatie tussen n delen opgebroken wordt, ook in n losse delen blijft functioneren.
