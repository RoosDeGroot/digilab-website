---
title: Unieke adresseerbaarheid
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T14:00:47.964Z
description: "Dit gaat om het via API specifiek kunnen pinpointen van een
  object, bijvoorbeeld met een URI: hoe maken we keuzes die voor iedereen
  werken?"
tags: []
---
Dit gaat om het via API specifiek kunnen pinpointen van een object, bijvoorbeeld met een URI (https://nl.wikipedia.org/wiki/Uniform_resource_identifier) Het vraagstuk is daarbij niet hoe het opgelost kan worden, maar juist hoe we keuzes maken die voor iedereen werken. Er zijn bijvoorbeeld organisaties die menen dat je geen BSN mag gebruiken in een URI, en anderen die dat wel doen. En er zijn organisaties die een POST-methode gebruiken in plaats van GET omdat men denkt dat gevoelige informatie dan beter beschermd is dan in de URI. Gaat om het maken van keuzes en dus standaardisatie.
