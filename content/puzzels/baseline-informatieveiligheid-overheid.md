---
title: Baseline Informatieveiligheid Overheid
draft: false
puzzel-categorie: Vertrouwen > Veiligheid
date: 2023-11-22T10:23:09.753Z
description: "De Baseline Informatieveiligheid Overheid wordt binnen de gehele
  overheid gebruikt als basis voor veiligheid, eenduidigheid en kostenbesparing.
  "
tags: []
---
De Baseline Informatieveiligheid Overheid wordt binnen de gehele overheid gebruikt als basis voor veiligheid, eenduidigheid en kostenbesparing. Welke best practices kunnen organisaties helpen om te voldoen aan deze baseline? En is er een validator om deze compliancy te testen?
