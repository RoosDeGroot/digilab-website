---
title: "Projecten in Digilab"
description: "Projecten en beproevingen in Digilab"
date: 2023-08-15T10:35:34+02:00
draft: false
images: []
---

Digilab werkt aan verschillende projecten. Dat kunnen projecten zijn die een overheidsorganisatie in samenwerking met ons uitvoert, maar we hebben ook een goed gevulde backlog met eigen beproevingen.
